# Vemco Group guidelines

Guidelines used at our company


## ESLint

We use the eslint vue-plugin with the recommended settings.

[https://github.com/vuejs/eslint-plugin-vue](https://github.com/vuejs/eslint-plugin-vue)

To run the liner simply run

```
yarn lint path/to/js/assets
```

to have it autofix your files run command with **--fix**


## Code Style

### Javascript

For Phpstorm use Code Style / Javascript / Set From / Predefined Style / JavaScript standard style.
Then set the following:
*  Set: **4** for tab size, indent & continuation indent
*  Disable: Spaces / Before Parantheses > **Function decleration parantheses**
*  Remove: Wrapping and Braces > Hard wrap at 80 (clear box)
*  Set: **Add when multiline** for Punctuation > Trailing comma
  

### HTML

*  Set: **4** for tab size, indent & continuation indent
*  Set: Other > Wrap attributes: **Wrap always**
*  Remove **thead, tbody, tfoot** from Do not indent children of
*  Set: New line before first attribute: **When multiline**
*  Enable: Other > Spaces > **In empty tag**
*  Set: Other > New line after last attribute: **When multiline**


### PHP

For Phpstorm use Code Style / PHP / Set From / Predefined Style / Laravel.
* Set: Hard wrap at **160**
* Enable: Spaces > Around Operators > **Concatenation**
* Set: Code Conversion / Sort use statements: **By length**
* Set: Editor / General / Auto Import
  * Function: prefer import


## GIT PRE-COMMIT
```
#!/bin/bash
YARN_LINT="yarn lint"
PHPCS="./vendor/bin/phpcs --extensions=php --ignore=deploy.php,.phpstorm.meta.php,_ide_helper.php,_ide_helper_models.php,/database/,/tests/,/resources/,/config/,deploy.php,/vendor/,/node_modules/,/storage/,/public/,/tools/,/bootstrap/ --colors -s --standard=./resources/server/build/rules/phpcs.xml"
PHPCS_TEST="./vendor/bin/phpcs --extensions=php --colors -s --standard=./resources/server/build/rules/phpcs_tests.xml"
FILES=$(git diff --staged --name-only --diff-filter=ACMR HEAD)

if [ "$FILES" == "" ]; then
    exit 0
fi

SELECTED_FILES=""
TEST_FILES=""
ASSET_FILES=""

for FILE in $FILES
do
  if [[ $FILE =~ "tests/" ]]
  then
    TEST_FILES="$TEST_FILES $FILE"
  elif [[ $FILE =~ "resources/assets/js/components" ]]
  then
    ASSET_FILES="$ASSET_FILES $FILE"
  else
    SELECTED_FILES="$SELECTED_FILES $FILE"
  fi
done

if [[ ! -z $SELECTED_FILES ]]
then
  OUTPUT=$($PHPCS $SELECTED_FILES)
  RETVAL=$?

  if [ $RETVAL -ne 0 ]; then
    echo "$OUTPUT" | more
    exit $RETVAL
  fi
fi

if [[ ! -z $ASSET_FILES ]]
then
  OUTPUT=$($YARN_LINT $ASSET_FILES)
  RETVAL=$?

  if [ $RETVAL -ne 0 ]; then
    echo "$OUTPUT" | more
    exit $RETVAL
  fi
fi

if [[ ! -z $TEST_FILES ]]
then
  OUTPUT=$($PHPCS_TEST $TEST_FILES)
  RETVAL=$?

  if [ $RETVAL -ne 0 ]; then
    echo "$OUTPUT" | more
    exit $RETVAL
  fi
fi

exit 0
```



>  Inspired by https://github.com/spatie/guidelines.spatie.be